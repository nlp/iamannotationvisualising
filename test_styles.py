#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 07:27:27 2022

@author: gozat
"""

font_style = {'normal':0, 'bold':1, 'italic':3, 'underligned':4, 'blinking':5,
              'background':7}
colors = {'black':0, 'red':1, 'green':2, 'yellow':3, 'blue':4, 'purple':5, 'cyan':6, 'white':7}

for font_style in range(8):
    style = chr(27)+"["+str(font_style)+";37;40m"
    nostyle = chr(27)+"[m"
    print(str(font_style) + chr(32) + style+"Normal text"+nostyle)

for i in range(8):
    style = chr(27)+"[6"+";3"+str(i)+";40m"
    nostyle = chr(27)+"[m"
    print(str(i) + chr(32) + style+"Normal text"+nostyle)

for i in range(8):
    style = chr(27)+"[6"+";30;4"+str(i)+"m"
    nostyle = chr(27)+"[m"
    print(str(i) + chr(32) + style+"Normal text"+nostyle)

for i in range(8):
    style = chr(27)+"[1"+";4"+str(i)+"m"
    nostyle = chr(27)+"[m"
    print(str(i) + chr(32) + style+"Normal text"+nostyle)
