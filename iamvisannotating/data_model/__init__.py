#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Import Annotation and AnnotatedText
"""

from .annotated_text import AnnotatedText
from .annotation import Annotation
