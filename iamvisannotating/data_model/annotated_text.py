#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
AnnotatedText class represents a text (a string) and a list of annotations (collection of start, stop and label).
"""

from typing import List
from pydantic import BaseModel

from.annotation import Annotation

class AnnotatedText(BaseModel):
    text: str
    annotations: List[Annotation]
