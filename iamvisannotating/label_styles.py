#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Deals with the font styles. 

Generates the codes of the print.
See https://ozzmaker.com/add-colour-to-text-in-python/ for examples.

The general pattern (seems to be):
    ```python
    "\033["+str(font_style)+";3"+str(font_color)+";4"+str(background_color)+"m"
    ```
where 
  - `\033` is `chr(27)` 
  - the `font_style` is chosen in the `styles` mapping below, 
  - the `font_color` and the `background_color` are chosen in the `colors` mapping below.

Here, we choose to fix the font color on black, to change the background color, and to use bold representation.

"""

style_end = chr(27) + "[0m"
# styles = {0:'normal', 1:'bold', 3:'italic', 4:'underligned', 5:'blinking'}
# colors = {0:'black', 1:'red', 2:'green', 3:'yellow', 4:'blue', 5:'purple', 6:'cyan', 7:'white'}


class FontStyles(object):
    """Infinite generator of colors and styles."""

    i = 0

    def __next__(self):
        style = chr(27) + "[1;30" + ";4" + str(self.i + 1) + "m"
        self.i = 0 if self.i // 6 else self.i + 1
        return style

    def __iter__(self):
        return self


class LabelStyles(dict):
    """Dictionary that represents the labels associated to some font style."""

    fontstyles = FontStyles()

    def __missing__(self, label):
        style = next(self.fontstyles)
        super().__setitem__(label, style)
        return self[label]
