#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Visualisation of annotation. Most of the methods are just manipulations of strings.
"""

from .data_model import AnnotatedText
from .label_styles import LabelStyles, style_end

label_styles = LabelStyles()


def style_text(text, annotation, start):
    """Decorate a text with the style of the annotation label with respect to the label_styles global dictonnary."""
    global label_styles
    string = label_styles[annotation.label]
    string += text[start:annotation.stop]
    string += style_end
    return string


def legend(annotated_text: AnnotatedText):
    """Construct the string of the legend associated to the AnnotatedText."""
    label_set = set()
    labels, examples = [], []
    for annotation in annotated_text.annotations:
        label = annotation.label
        if label not in label_set:
            labels.append(' '.join(lab.title() for lab in label.split('_')))
            examples.append(style_text(annotated_text.text,
                                       annotation,
                                       annotation.start))
            label_set.add(label)
    lenmax = max(len(lab) for lab in labels) + 2
    string = "Label".rjust(lenmax) + ' : example\n'
    string += '-' * (lenmax + max(len(ex) for ex in examples)) + '\n'
    for label, example in zip(labels, examples):
        string += label.rjust(lenmax)
        string += ' : '
        string += example
        string += '\n'
    return string


def annotations(annotated_text: AnnotatedText) -> str:
    """Construct the string from the Annotated Text."""
    text = str(annotated_text.text)
    annotations = annotated_text.annotations.copy()
    annotations = sorted(annotations, key=lambda x: (x.start, x.stop))
    startemp, string = 0, str()
    for annotation in annotations:
        if annotation.start < startemp:
            start = startemp
        else:
            start = annotation.start
        string += text[startemp:start]
        string += style_text(text, annotation, start)
        startemp = annotation.stop
    string += text[startemp:]
    return string
