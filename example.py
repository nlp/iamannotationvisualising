#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example of visualisation. 

Run this script as 

```bash
python3 example.py
```
"""

import re

from iamvisannotating import AnnotatedText, Annotation, visualise

text = """Of course, this is not how you use iterators in practice. I’m just demonstrating how they work internally. If you ever need to manually get an iterator, use the iter() function instead. And if you need to manually call the __next__ method, you can use Python’s next() function.\n -- extracted from https://python.land/deep-dives/python-iterator on Nov.17/2022"""

annotations = [Annotation(start=r.start(), stop=r.end(), label='iterator match')
               for r in re.finditer('iterator', text)]
annotations += [Annotation(start=r.start(), stop=r.end(), label='iter match')
                for r in re.finditer('iter', text)]
annotations += [Annotation(start=r.start(), stop=r.end(), label='next match')
                for r in re.finditer('next', text)]
annotations += [Annotation(start=r.start(), stop=r.end(), label='course match')
                for r in re.finditer('course', text)]
annotations += [Annotation(start=r.start(), stop=r.end(), label='https match')
                for r in re.finditer('https', text)]
annotations += [Annotation(start=r.start(), stop=r.end(), label='manual match')
                for r in re.finditer('manual', text)]
annotations += [Annotation(start=r.start(), stop=r.end(), label='extracted match')
                for r in re.finditer('extracted', text)]
annotations += [Annotation(start=r.start(), stop=r.end(), label='function match')
                for r in re.finditer('function', text)]
annotations += [Annotation(start=r.start(), stop=r.end(), label='punctuation regex')
                for r in re.finditer(r'\W+', text)]
annotations += [Annotation(start=r.start(), stop=r.end(), label='digits regex')
                for r in re.finditer(r'\d+', text)]

annotated_text = AnnotatedText(text=text, annotations=annotations)

annotated_string = visualise.annotations(annotated_text)
legend_string = visualise.legend(annotated_text)

print(annotated_string)
print('\n' + '#' * 79 + '\n')
print(legend_string)
