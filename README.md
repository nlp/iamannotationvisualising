# iamVisAnnotating



## Getting started

Clone the repository: 

```bash
git clone 
```

Then run the example:

```bash
python3 example.py
```

This should give something like

![Example screenshot](documentation/fig/example_screenshot.jpg "Example of visualisation")